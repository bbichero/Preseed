#!/bin/bash

library_path=".."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/system_functions.sh" ]
then
	# Include library
	. ${library_path}/shell_library/type_functions.sh
	. ${library_path}/shell_library/system_functions.sh
else
	echo "Can't load library files, abort"
	exit 1
fi


